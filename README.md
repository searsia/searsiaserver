Searsia Server Version 1
========================
https://searsia.org

Usage:
+ Build with: `mvn package`
+ Run with: `java -jar target/searsiaserver.jar -m <url>`
+ Done.

The option `-m` is required: It connects your server to an
existing Searsia server, see [Searsia server options][1].
Connect to your server with the example [Federated Web Search Client][2].
More information can be found in the [Searsia Documentation][3],
or you may ask a question under [Searsia Server Issues][4].
The server is tested on Ubuntu 20.04 and 22.04.

[1]: https://searsia.org/start.html#server
[2]: https://codeberg.org/searsia/searsiaclient "Searsia Client"
[3]: https://searsia.org "Searsia Documentation"
[4]: https://codeberg.org/searsia/searsiaserver/issues "Issues"
